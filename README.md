##AnimatedListView
This is a merged project that contains all features from JazzyListView and Android-PullToRefreshView.  
It provides a list and grid view widget in one library.
###Usage
You can use the animated view in different ways:
 
*  default behavior as used in most Android applications
*  as *animated* list/grid view widget
*  as *pull to refresh* list/grid view widget
*  combined both *pull to refresh* and *animated*
###Using the view widget in a layout  
    <?xml version="1.0" encoding="utf-8"?>
    <RelativeLayout 
      android:layout_width="match_parent"
      android:layout_height="match_parent"
    >
      <com.phoenixandroid.widget.animatedlistview.AnimatedListView
        android:id="@+id/list"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
      />
    </RelativeLayout>  
*You don't need to wrap the view with relative layout, it's only a example*
###Using the view widget in your code (Basic usage) 
```java
import com.phoenixandroid.widget.animatedlistview.AnimatedListView;

class MainAcivity extends Activity {
    
  private AnimatedListView mList;
  private SampleAdapter mAdapter;  
      
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_layout);
        
    mList = (AnimatedListView) findViewById(R.id.list);
    mAdapter = new SampleAdapter();
    mList.setAdapter(mAdapter);
  }
}
```
###Using the view widget in your code (Animated usage) 
```java
import com.phoenixandroid.widget.animatedlistview.AnimatedListView;
import com.phoenixandroid.widget.animatedlistview.AnimatedListViewHelper;

class MainAcivity extends Activity {
    
  private AnimatedListView mList;
  private SampleAdapter mAdapter;  
      
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_layout);
        
    mList = (AnimatedListView) findViewById(R.id.list);
    // set animation type
    mList.setTransitionEffect(AnimatedListViewHelper.HELIX);
    mAdapter = new SampleAdapter();
    mList.setAdapter(mAdapter);
  }
}
```
###Using the view widget in your code (PullToRefresh usage) 
```java
import com.phoenixandroid.widget.animatedlistview.AnimatedListView;
import com.phoenixandroid.widget.animatedlistview.AnimatedListViewHelper;

class MainAcivity extends Activity {
    
  private AnimatedListView mList;
  private SampleAdapter mAdapter;  
      
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_layout);
        
    mList = (AnimatedListView) findViewById(R.id.list);
    // set refresh type
    mList.setMode(Mode.PULL_FROM_START);
    mAdapter = new SampleAdapter();
    mList.setAdapter(mAdapter);
  }
}
```
###Using the view widget in your code (Both animated and PullToRefresh usage) 
```java
import com.phoenixandroid.widget.animatedlistview.AnimatedListView;
import com.phoenixandroid.widget.animatedlistview.AnimatedListViewHelper;

class MainAcivity extends Activity {
    
  private AnimatedListView mList;
  private SampleAdapter mAdapter;  
      
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_layout);
        
    mList = (AnimatedListView) findViewById(R.id.list);
    // set refresh type
    mList.setMode(Mode.PULL_FROM_START);
    // set animation type
    mList.setTransitionEffect(AnimatedListViewHelper.HELIX);
    mAdapter = new SampleAdapter();
    mList.setAdapter(mAdapter);
  }
}
```