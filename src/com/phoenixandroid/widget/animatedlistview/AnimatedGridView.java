package com.phoenixandroid.widget.animatedlistview;

import com.phoenixandroid.widget.animatedlistview.PullToRefreshGridView;

import android.content.Context;
import android.util.AttributeSet;

public class AnimatedGridView extends PullToRefreshGridView {

    private final AnimatedListViewHelper mHelper;

    public AnimatedGridView(Context context) {
        super(context);
        mHelper = new AnimatedListViewHelper(context, null);
        super.setOnScrollListener(mHelper);
    }

    public AnimatedGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mHelper = new AnimatedListViewHelper(context, attrs);
        super.setOnScrollListener(mHelper);
    }

    /**
     * Sets the desired transition effect.
     *
     * @param transitionEffect Numeric constant representing a bundled transition effect.
     */
    public void setTransitionEffect(int transitionEffect) {
        mHelper.setTransitionEffect(transitionEffect);
    }

    /**
     * Sets the desired transition effect.
     *
     * @param transitionEffect The non-bundled transition provided by the client.
     */
    public void setTransitionEffect(AnimatedListViewEffect transitionEffect) {
        mHelper.setTransitionEffect(transitionEffect);
    }

    /**
     * Sets whether new items or all items should be animated when they become visible.
     *
     * @param onlyAnimateNew True if only new items should be animated; false otherwise.
     */
    public void setShouldOnlyAnimateNewItems(boolean onlyAnimateNew) {
        mHelper.setShouldOnlyAnimateNewItems(onlyAnimateNew);
    }
}
