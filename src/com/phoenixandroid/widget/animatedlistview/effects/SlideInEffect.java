package com.phoenixandroid.widget.animatedlistview.effects;

import android.view.View;

import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.phoenixandroid.widget.animatedlistview.AnimatedListViewEffect;

public class SlideInEffect implements AnimatedListViewEffect {
    @Override
    public void initView(View item, int position, int scrollDirection) {
        ViewHelper.setTranslationY(item, item.getHeight() / 2 * scrollDirection);
    }

    @Override
    public void setupAnimation(View item, int position, int scrollDirection, ViewPropertyAnimator animator) {
        animator.translationYBy(-item.getHeight() / 2 * scrollDirection);
    }
}
