package com.phoenixandroid.widget.animatedlistview.effects;

import android.view.View;

import com.nineoldandroids.view.ViewPropertyAnimator;
import com.phoenixandroid.widget.animatedlistview.AnimatedListViewEffect;

public class StandardEffect implements AnimatedListViewEffect {

    @Override
    public void initView(View item, int position, int scrollDirection) {
        // no op
    }

    @Override
    public void setupAnimation(View item, int position, int scrollDirection, ViewPropertyAnimator animator) {
        // no op
    }

}
