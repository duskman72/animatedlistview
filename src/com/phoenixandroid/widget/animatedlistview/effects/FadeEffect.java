package com.phoenixandroid.widget.animatedlistview.effects;

import android.view.View;

import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.phoenixandroid.widget.animatedlistview.AnimatedListViewHelper;
import com.phoenixandroid.widget.animatedlistview.AnimatedListViewEffect;

public class FadeEffect implements AnimatedListViewEffect {

    private static final int DURATION_MULTIPLIER = 5;

    @Override
    public void initView(View item, int position, int scrollDirection) {
        ViewHelper.setAlpha(item, AnimatedListViewHelper.TRANSPARENT);
    }

    @Override
    public void setupAnimation(View item, int position, int scrollDirection, ViewPropertyAnimator animator) {
        animator.setDuration(AnimatedListViewHelper.DURATION * DURATION_MULTIPLIER);
        animator.alpha(AnimatedListViewHelper.OPAQUE);
    }

}
